/* we can also have an array of objects. We can group together object in an array.
that array can also use array methods for the objects. however, being objects are more complex than strings or numbers, there is a difference when handling them.
*/

let users = [
	{
		name: "Mike Shell",
		username: "mikeBlueShell",
		email: "mikeyShell01@gmail.com",
		password: "iamikey1999",
		isActive: true,
		dateJoined: "August 8, 2011"

	},
	{
		name: "Jake Janella",
		username: "jjnella99",
		email: "jakekanella_99@hgmail.com",
		password: "jackiejackie2",
		isActive: true,
		dateJoined: "January 14, 2015"
	}

];

console.log(users[0]);
// how we can access the property of our in an array?
// show the second items' email property:

console.log(users[1].email);

// Can we update the properties of objects in an array?
/*
	Update the emails of both user:
	email: "mikeyShell01@gmail.com"
	email: "jakekanella_99@hgmail.com"
*/	

users[0].email = "mikeKingOfShells@gmail.com";
users[1].email = "janellajakeArchitect@gmail.com";

console.log(users);

// can we also use array methods for arrau of objects?

// add another user into the array: push
users.push({
	name: "James Jameson",
	username: "iHateSpidey",
	email: "jamesJjameson@gmail.com",
	password: "spideyisamenace64",
	isActive: true,
	dateJoined: "Febuary 14, 2021"
});

console.log(users);

// this keyword refers to the object it belong
class User{
	constructor(name, username, email, password){
		this.name = name;
		this.username = username;
		this.email = email;
		this.password = password;
		this.isActive = true;
		this.dateJoined = "September 28, 2021";
	}
}

let newUser1 = new User ("Kate Middleton", "notTheDucheess", "serioulyNotDutchess@gmail.com", "notRoyaltyAtAll");

console.log(newUser1);

users.push(newUser1);

console.log(users);


// find() is able to return the found item

function login(username, password) {
	let userFound = users.find((user) => {

		if(user.username !==username && user.password !== password){
			console.log(`${username} Thankyou for loggin in`)
		}
		else if (user.username !== username && user.password !==password){
			return;
		}
		else {
			console.log(`Login Failed. Invalid Credentials`)
		}
		return user.username === username && user.password === password
	})
};
		

login("mikeBlueshell", "iammikey1000")
login("notTheDuchess", "notRoyaltyAtAll");
login("notTheDuchess", "notRoyaltyAtAll");
login("notTheDuchess", "notRoyaltyAtAll");


	

		


		

	
	
		
